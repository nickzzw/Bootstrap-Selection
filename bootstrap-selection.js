/**
 * Copyright (c) 2014, packy-tang V0.1.1
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

!function($){

	/**
	 * 初始化选择项 控件
	 * @param {[type]} el [description]
	 */
	var Selection = function(el){
		this.$el = $(el);
		this.el = el;
		this.temp = _.template('<li><label for="<%- id %>" class="checkbox"><input id="<%- id %>" name="<%- name %>" type="checkbox" value="<%- value %>"><%- content %></label></li>');
		this.sourceDefault = {
			'id':'',
			'name':'',
			'value':'',
			'content':''
		};

		this.title(this.$el.attr('data-title'));
		this.$el.on('change','[type=checkbox]',this,function(e){e.data.onChange(e,this)});

		if(!$.fn.dropdown.Constructor) return;
		this.initDropdownModule();
	};

	Selection.prototype = new $.fn.dropdown.Constructor();//继承Bootstrap.dropdown类
	/**
	 * 标题
	 * @param  {string} title 没输入值：取得标题;有输入值：设置标题
	 * @return {string}       标题
	 */
	Selection.prototype.title = function(title) {
		if(!!title)
			this.$el.find('.dropdown-toggle').html(title);
		else
			return this.$el.find('.dropdown-toggle').text();
	};
	/**
	 * 重设Dropdown设置
	 * @return {[type]} [description]
	 */
	Selection.prototype.initDropdownModule = function() {
		this.$el.on('click','.dropdown-menu',function(e){
			e.stopPropagation();
		});
	};
	/**
	 * 全选
	 * @return {[type]} [description]
	 */
	Selection.prototype.all = function() {
		this.items().each(function(){
			$(this).content().checked = true;
		});
	};
	/**
	 * 反选
	 * @return {[type]} [description]
	 */
	Selection.prototype.invert = function() {
		this.items().each(function(){
			$(this).content().checked = !$(this).content().checked;
		});
	};
	/**
	 * 取消选择
	 * @return {[type]} [description]
	 */
	Selection.prototype.none = function() {
		$(this).content().checked = false;
	};
	/**
	 * 数据原
	 * @param  {JSON} data     数据
	 * @param  {string} template 选择项模板
	 * @return {JSON}          数据
	 */
	Selection.prototype.source = function(data,template) {
		if(!data) return this._source;

		var data = this._source = data;
		this.temp = !!template?_.template(template):this.temp;

		this.$el.find('.dropdown-menu').empty();
		for(var i in data)
			$(this.temp($.extend({},this.sourceDefault,data[i]))).appendTo(this.$el.find('.dropdown-menu')).data('bs.selection.item.data',data[i]);
	};
	/**
	 * 选择项改变时
	 * @param  {Event} e      事件对象
	 * @param  {Object} target 当前改变项的DOM
	 * @return {[type]}        [description]
	 */
	Selection.prototype.onChange = function(e,target) {
		this.$el.trigger(e = $.Event('bs.selection.changed', {'relatedTarget':target}),this,target);
	};
	/**
	 * 选择项
	 * @return {JQuery} 选择项的JQuery对象
	 */
	Selection.prototype.items = function() {
		return this.$el.find('[type=checkbox]');
	};
	
	/**
	 * 插件化 选择项控件
	 * @param  {JOSN} options 可选项
	 * @return {[type]}         [description]
	 */
	$.fn.selection = function(){
		var options = Array.prototype.shift.apply(arguments);
		var arg = arguments;
		return this.each(function(){
			var data = $(this).data('bs.selection');
			var source = $(this).data('source');
			if(!data) $(this).data('bs.selection',(data = new Selection(this)));
			if(!!source) data.source(source);
			if(typeof options == 'string') data[options].apply(data,arg);
		});
	};
	$.fn.selection.Constructor = Selection;


}(jQuery);