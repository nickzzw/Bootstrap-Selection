#Bootstrap Selection Beta V0.1.1

基于Bootstrap.dropdown来制作的一个下拉多选控件
使用前需加载`JQuery`、`Bootstrap库（样式和JS）`还有`underscore js`。

*如果不需要动态加载下拉项可不加载underscore js*

##例子
	
例子

    <div class="dropdown dropdown-checkbox" data-target="dropdown-checkbox">
	    <a href="#" id="drop2" role="button" class="dropdown-checkbox-toggle dropdown-toggle" data-toggle="dropdown">星级 <b class="caret"></b></a>
	    <ul class="dropdown-menu dropdown-checkbox-menu" role="menu" aria-labelledby="drop2">
		    <li><label for="1" class="checkbox"><input id="1" name="star" type="checkbox" value="">五星</label></li>
		    <!-- ... -->
		    <li><label for="8" class="checkbox"><input id="8" name="star" type="checkbox" value="">经济型</label></li>
	    </ul>
    </div>

	<!-- 你还需要加载bootstrap样式和JS -->
	<link href="bootstrap-selection.css" rel="stylesheet">
	<script type="text/javascript" src="http://libs.baidu.com/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript" src="underscore-min.js"></script>
    <script type="text/javascript" src="bootstrap-selection.js"></script>
    <script type="text/javascript">
		$('[data-target=dropdown-checkbox]').selection()
    </script>

##方法

显示（或隐藏）下拉框	
`$('.dropdown-checkbox').selection('toggle')`

全选	
`$('.dropdown-checkbox').selection('all')`

反选	
`$('.dropdown-checkbox').selection('invert')`

取消选择	
`$('.dropdown-checkbox').selection('none')`

取得标题	
`$('.dropdown-checkbox').selection('title')`

设置标题	
`$('.dropdown-checkbox').selection('title','标题')`

取得数据源	    
`$('.dropdown-checkbox').selection('source')`

设置数据源	    
`$('.dropdown-checkbox').selection('source',{JSON})`

取得所有下拉项的JQuery对象(就是所有checkbox)	
`$('.dropdown-checkbox').selection('items')`



##事件
多选控件勾选后促发bs.selection.changed事件。

	$('.dropdown-checkbox').selection().on('bs.selection.changed',function(e,selection,$item){
		//...
	});

##其他
在dropdown-checkbox元素中设置以下`data-`属性会自动加载
	
	 data-title='星级 <b class="caret"></b>'
	 data-source='[{"id":1,"name":"star","content":"五星"},...,{"id":8,"name":"star","content":"经济型"}]'

取得每个子项的数据源，前提是动态加载下拉的子项

	$('.dropdown-checkbox').selection('items').each(function(){
		//数据保存在li上
		var data = $(this).parents('li').data('bs.selection.item.data');
	});
    
项目新版本已经使用bower、gruntjs等管理工具从新打包，多谢各位关注。
由于我觉得coding.net有趣一点，把项目迁移到coding了，地址如下：
> [https://coding.net/u/packy/p/Bootstrap-Selection/git][2]

  [2]: https://coding.net/u/packy/p/Bootstrap-Selection/git